/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.itsronald.widget.utils;

import ohos.agp.components.AttrSet;

import ohos.app.Context;

public final class AttrUtils {

    private AttrUtils() {
    }

    /**
     * 属性.
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认属性
     * @return 自定义属性
     */
    public static String getStringFromAttr(final AttrSet attrSet,
                                           final String name,
                                           final String defaultValue) {
        String value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null
                    && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getStringValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 属性.
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认属性
     * @return 自定义属性
     */
    public static Integer getDimensionFromAttr(final AttrSet attrSet,
                                               final String name,
                                               final Integer defaultValue) {
        Integer value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null
                    && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getDimensionValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 属性.
     *
     * @param context      上下文
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认属性
     * @return 自定义属性
     */
    public static Integer getInteger(final Context context,
                                     final AttrSet attrSet,
                                     final String name,
                                     final Integer defaultValue) {
        Integer value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null
                    && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 属性.
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认属性
     * @return 自定义属性
     */
    public static int getColor(final AttrSet attrSet,
                               final String name,
                               final int defaultValue) {
        int value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null
                    && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 属性.
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认属性
     * @return 自定义属性
     */
    public static Integer getIntegerFromAttr(final AttrSet attrSet,
                                             final String name,
                                             final Integer defaultValue) {
        Integer value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null
                    && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 属性.
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认属性
     * @return 自定义属性
     */
    public static float getFloatFromAttr(final AttrSet attrSet,
                                         final String name,
                                         final float defaultValue) {
        float value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null
                    && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getFloatValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }
    /**
     * 属性.
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认属性
     * @return 自定义属性
     */
    public static boolean getFloatFromAttr(final AttrSet attrSet,
                                           final String name,
                                           final boolean defaultValue) {
        boolean value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null
                    && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 属性.
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认属性
     * @return 自定义属性
     */
    public static Long getLongFromAttr(final AttrSet attrSet,
                                       final String name,
                                       final Long defaultValue) {
        Long value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null
                    && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getLongValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * 属性.
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认属性
     * @return 自定义属性
     */
    public static float getColorFromAttr(final AttrSet attrSet,
                                         final String name,
                                         final float defaultValue) {
        float value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null
                    && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }


    /**
     * 属性.
     *
     * @param attrSet      属性集
     * @param name         属性名
     * @param defaultValue 默认属性
     * @return 自定义属性
     */
    public static int getColorFromAttr(final AttrSet attrSet,
                                       final String name,
                                       final int defaultValue) {
        int value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null
                    && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

}
