/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.itsronald.widget.utils;


import java.math.BigDecimal;

import java.text.DecimalFormat;


public final class Utils {

    private Utils() {
    }

    /**
     * 计算。
     * @param f1 参数1
     * @param f2 参数2
     * @return  计算值
     */
    public static Float floatToSubtract(final float f1, final float f2) {
        BigDecimal bigDecimal1 = new BigDecimal(f1);
        BigDecimal bigDecimal2 = new BigDecimal(f2);
        DecimalFormat df = new DecimalFormat("0.00");
        return Float.valueOf(df.format(bigDecimal1.subtract(bigDecimal2)));
    }
}
