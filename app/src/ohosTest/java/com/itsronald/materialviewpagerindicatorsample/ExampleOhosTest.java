
package com.itsronald.materialviewpagerindicatorsample;

import com.itsronald.widget.utils.Utils;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest extends AbilitySlice {

    /**
     * 测试包名。
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.itsronald.materialviewpagerindicatorsample", actualBundleName);
    }

    /**
     * 测试计算
     */
    @Test
    public void testFloatToSubtract() {
        float testA=1.0F;
        float testB=0.0F;
        float expected =(testA-testB);
        Float actual = Utils.floatToSubtract(testA, testB);
        Assert.assertNotNull(actual);
        Assert.assertEquals(expected, actual, 0);
    }
}