package com.itsronald.materialviewpagerindicatorsample.slice;

import com.itsronald.materialviewpagerindicatorsample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button simpleJavaExampleAbtivity =
                (Button) findComponentById(ResourceTable.Id_simpleJavaExampleAbtivity);
        Button simpleXmlExampleAbtivity =
                (Button) findComponentById(ResourceTable.Id_simpleXmlExampleAbtivity);
        Button pageSliderDemoAbilitySlice =
                (Button) findComponentById(ResourceTable.Id_pageSliderDemoAbilitySlice);

        simpleXmlExampleAbtivity.setClickedListener(component -> {
            present(new SimpleXmlExampleAbtivity(),
                    new Intent().setParam("name",
                            "SimpleXmlExampleAbtivity"));
        });
        simpleJavaExampleAbtivity.setClickedListener(component -> {
            present(new SimpleJavaExampleAbtivity(),
                    new Intent().setParam("name",
                            "SimpleJavaExampleAbtivity"));
        });
        pageSliderDemoAbilitySlice.setClickedListener(component -> {
            present(new PageSliderDemoAbilitySlice(),
                    new Intent().setParam("name",
                            "PageSliderDemoAbilitySlice"));
        });


    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
