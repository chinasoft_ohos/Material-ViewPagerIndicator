package com.itsronald.materialviewpagerindicatorsample.slice;



import com.itsronald.materialviewpagerindicatorsample.ResourceTable;
import com.itsronald.widget.ViewPagerIndicator;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;

/**
 * @Author XiaoMing
 * @Time 2021/3/12-9:59
 * @Describe
 */
public class SimpleXmlExampleAbtivity extends BaseSlice {
    private int mCurrentPosition = 0;
    private PageSlider pageSlider;
    private ViewPagerIndicator pageSliderIndicator;

    private void initPageSliderIndicator() {
        pageSliderIndicator.setViewPager(pageSlider);
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_xml_view_pager);
        pageSliderIndicator = (ViewPagerIndicator) findComponentById(ResourceTable.Id_view_pager_indicator);

        initPageSlider();
        initPageSliderIndicator();


    }

    @Override
    public int getChildUIContent() {
        return ResourceTable.Layout_simple_xml_example;
    }

    private void initPageSlider() {
        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        DependentLayout layoutOne = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);
        DependentLayout layoutTwo = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);
        DependentLayout layoutThree = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);
        DependentLayout layoutFour = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);
        DependentLayout layoutFive = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);

        ArrayList<Component> pageView = new ArrayList();

        pageView.add(layoutOne);
        pageView.add(layoutTwo);
        pageView.add(layoutThree);
        pageView.add(layoutFour);
        pageView.add(layoutFive);

        pageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return pageView.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(pageView.get(i));
                return pageView.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent(pageView.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
        pageSlider.setCurrentPage(mCurrentPosition);
        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {
            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {
                DependentLayout component = (DependentLayout) pageView.get(i);
                Text textContent = (Text) component.findComponentById(ResourceTable.Id_textContent);
                textContent.setText(getString(ResourceTable.String_textContent, i));
            }
        });
    }



    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
