package com.itsronald.materialviewpagerindicatorsample.slice;

import com.itsronald.materialviewpagerindicatorsample.ResourceTable;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Rect;

import java.util.ArrayList;

public class PageSliderDemoAbilitySlice extends BaseSlice {

    private int mCurrentPosition = 0;
    private PageSlider pageSlider;
    private PageSliderIndicator pageSliderIndicator;
    private ShapeElement normalElement;
    private ShapeElement selectElement;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        pageSlider = (PageSlider) findComponentById(ResourceTable.Id_pageslider);
        pageSliderIndicator = (PageSliderIndicator) findComponentById(ResourceTable.Id_pagesliderindicator);

        initPageSlider();
        initPageSliderIndicator();
    }

    @Override
    public int getChildUIContent() {
        return ResourceTable.Layout_ability_pageslider_demo;
    }

    private void initPageSliderIndicator() {
        pageSliderIndicator.setViewPager(pageSlider);

        normalElement = new ShapeElement();
        normalElement.setShape(ShapeElement.OVAL);
        normalElement.setBounds(new Rect(0, 0, 20, 20));
        normalElement.setShaderType(ShapeElement.LINEAR_GRADIENT_SHADER_TYPE);
        normalElement.setRgbColor(RgbColor.fromArgbInt(0x80FFFFFF));

        selectElement = new ShapeElement();
        selectElement.setShape(ShapeElement.OVAL);
        selectElement.setBounds(new Rect(0, 0, 20, 20));
        selectElement.setShaderType(ShapeElement.LINEAR_GRADIENT_SHADER_TYPE);
        selectElement.setRgbColor(RgbColor.fromArgbInt(0xFFFFFFFF));

        pageSliderIndicator.setItemElement(normalElement, selectElement);
        pageSliderIndicator.setItemOffset(30);
    }

    private void initPageSlider() {
        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        DependentLayout layoutOne =
                (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one,
                        null, false);
        DependentLayout layoutTwo =
                (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one,
                        null, false);
        DependentLayout layoutThree =
                (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one,
                        null, false);
        DependentLayout layoutFour =
                (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one,
                        null, false);
        DependentLayout layoutFive =
                (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one,
                        null, false);

        ArrayList<Component> pageView = new ArrayList();
        pageView.add(layoutOne);
        pageView.add(layoutTwo);
        pageView.add(layoutThree);
        pageView.add(layoutFour);
        pageView.add(layoutFive);

        pageSlider.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return pageView.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(pageView.get(i));
                return pageView.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent(pageView.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
        pageSlider.setCurrentPage(mCurrentPosition);
        pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {
            }

            @Override
            public void onPageSlideStateChanged(int i) {
            }

            @Override
            public void onPageChosen(int position) {
                DependentLayout component = (DependentLayout) pageView.get(position);
                Text textContent = (Text) component.findComponentById(ResourceTable.Id_textContent);
                textContent.setText(getString(ResourceTable.String_textContent, position));
            }
        });
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


}
