package com.itsronald.materialviewpagerindicatorsample.slice;


import com.itsronald.materialviewpagerindicatorsample.ResourceTable;
import com.itsronald.widget.ViewPagerIndicator;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;

/**
 * @Author XiaoMing
 * @Time 2021/3/12-9:59
 * @Describe
 */
public class SimpleJavaExampleAbtivity extends BaseSlice {

   private PageSlider viewPager;
   private DependentLayout view;
    private int mCurrentPosition = 0;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        viewPager = (PageSlider) findComponentById(ResourceTable.Id_java_view_pager);
        view = (DependentLayout) findComponentById(ResourceTable.Id_container);
        initPageSlider();


        ViewPagerIndicator indicator = new ViewPagerIndicator(this);
        // 步骤4.2 将Text添加到布局中
        view.addComponent(indicator);
        indicator.setViewPager(viewPager);
    }

    @Override
    public int getChildUIContent() {
        return ResourceTable.Layout_simple_java_example;
    }

    private void initPageSlider() {
        LayoutScatter layoutScatter = LayoutScatter.getInstance(getContext());
        DependentLayout layoutOne = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);
        DependentLayout layoutTwo = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);
        DependentLayout layoutThree = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);
        DependentLayout layoutFour = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);
        DependentLayout layoutFive = (DependentLayout) layoutScatter.parse(ResourceTable.Layout_page_one, null, false);

        ArrayList<Component> pageView = new ArrayList();
        pageView.add(layoutOne);
        pageView.add(layoutTwo);
        pageView.add(layoutThree);
        pageView.add(layoutFour);
        pageView.add(layoutFive);

        viewPager.setProvider(new PageSliderProvider() {
            @Override
            public int getCount() {
                return pageView.size();
            }

            @Override
            public Object createPageInContainer(ComponentContainer componentContainer, int i) {
                componentContainer.addComponent(pageView.get(i));
                return pageView.get(i);
            }

            @Override
            public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
                componentContainer.removeComponent(pageView.get(i));
            }

            @Override
            public boolean isPageMatchToObject(Component component, Object o) {
                return component == o;
            }
        });
        viewPager.setCurrentPage(mCurrentPosition);
        viewPager.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

            }

            @Override
            public void onPageSlideStateChanged(int i) {

            }

            @Override
            public void onPageChosen(int i) {
                DependentLayout component = (DependentLayout) pageView.get(i);
                Text textContent = (Text) component.findComponentById(ResourceTable.Id_textContent);
                textContent.setText(getString(ResourceTable.String_textContent, i));
            }
        });
    }

}
