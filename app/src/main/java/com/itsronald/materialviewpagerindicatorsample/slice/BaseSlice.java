package com.itsronald.materialviewpagerindicatorsample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;

public abstract class BaseSlice extends AbilitySlice {

    @Override
    protected void onStart(final Intent intent) {
        super.onStart(intent);
        DirectionalLayout layout = new DirectionalLayout(getContext());
        int par = DirectionalLayout.LayoutConfig.MATCH_PARENT;
        int con = DirectionalLayout.LayoutConfig.MATCH_CONTENT;
        layout.setLayoutConfig(new DirectionalLayout.LayoutConfig(par, par));
        Component child = LayoutScatter.getInstance(getContext()).parse(getChildUIContent(), null, false);
        layout.addComponent(child);
        setUIContent(layout);

    }

    public abstract int getChildUIContent();
}
