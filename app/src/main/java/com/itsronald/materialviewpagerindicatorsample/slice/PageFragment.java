package com.itsronald.materialviewpagerindicatorsample.slice;

import com.itsronald.materialviewpagerindicatorsample.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;

/**
 * @Author XiaoMing
 * @Time 2021/3/12-10:46
 * @Describe
 */
public class PageFragment extends Fraction {
    private String pageText;

    private PageFragment(String pageText) {
        this.pageText=pageText;
    }

    public  static  PageFragment newInstance(final String pageText){
        PageFragment fragment = new PageFragment(pageText);
        return fragment;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component parse = scatter.parse(ResourceTable.Layout_page_item, null, false);
        Text mText = (Text) parse.findComponentById(ResourceTable.Id_text);
        if (mText != null) {
            mText.setText(pageText);
        }
        return parse;
    }
}
