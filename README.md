# Material-ViewPagerIndicator

## 项目介绍
项目名称：Material-ViewPagerIndicator

所属系列：openharmony的第三方组件适配移植

功能：适用于新系统 的超级易用的页面指示器，实现平移，显隐组合动画效果。

项目移植状态：主功能完成

调用差异：无

开发版本：sdk6，DevEco Studio2.2 beta1

基线版本：Release 1.0.4

## 效果演示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0521/114236_422ec0a1_1659014.gif "ViewPagerIndicator0.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0521/114249_a80b57e5_1659014.gif "ViewPagerIndicator1.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0521/114302_46d38b02_1659014.gif "ViewPagerIndicator2.gif")


## 安装教程

方式一：

1.在项目根目录下的build.gradle文件中，

 ```

allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:Material-ViewPagerIndicator:1.0.5')
    ......  
 }
 ```

在sdk6，DevEco Studio2.2 beta1下项目可直接运行 
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件,
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

## 使用说明

```xml
<com.itsronald.widget.ViewPagerIndicator
    ohos:id="$+id:view_pager_indicator"
    ohos:height="50vp"
    ohos:width="match_content"
    ohos:center_in_parent="true"
    ohos:align_parent_bottom="true"
    ohos:bottom_margin="50vp"
    ohos:horizontal_center="true"
    hap:ipi_animationDuration="420"
    hap:ipi_dotDiameter="40"
    hap:ipi_pageIndicatorColor="#80ffffff"
    hap:ipi_currentPageIndicatorColor="#ffffff"
    hap:ipi_dotGap="28"/>

``` 
Or in code...

```java
import com.itsronald.widget.ViewPagerIndicator;

...
   private PageSlider viewPager;
   private DependentLayout view;

  viewPager = (PageSlider) findComponentById(ResourceTable.Id_java_view_pager);
        view = (DependentLayout) findComponentById(ResourceTable.Id_container);

   ViewPagerIndicator indicator = new ViewPagerIndicator(this);
        view.addComponent(indicator);
        indicator.setViewPager(viewPager);

```

## 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代
 - 1.0.5
 - 0.0.1-SNAPSHOT

## 版权和许可信息

```
**Material-ViewPagerIndicator** is licensed under Apache 2.0.

    Copyright 2016 Ronald Martin

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    
Parts of this library are derived from Support-v4's [`PagerTitleStrip`](https://android.googlesource.com/platform/frameworks/support.git/+/master/v4/java/android/support/v4/view/PagerTitleStrip.java), which is also licensed under Apache 2.0.
